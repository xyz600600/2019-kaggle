#include "graph.hpp"

#include <iostream>

Graph::Graph(const std::size_t size)
    : neighbor_list_(size)
{
}

void Graph::connect(const std::size_t from, const std::size_t to, const bool bidir)
{
    neighbor_list_[from].insert(to);
    if (false)
    {
        neighbor_list_[to].insert(from);
    }
}

const auto& Graph::operator[](std::size_t index) const
{
    return neighbor_list_[index];
}

auto& Graph::operator[](std::size_t index)
{
    return neighbor_list_[index];
}

void Graph::to_graphviz(std::ostream& out, const std::vector<Coordinate<double>>& position_list) const
{
    const std::size_t size = 800;

    out << "digraph xyz_graph {" << std::endl;

    for (std::size_t i = 0; i < neighbor_list_.size(); i++)
    {
        const std::size_t x = static_cast<std::size_t>(size * position_list[i].x_);
        const std::size_t y = static_cast<std::size_t>(size * position_list[i].y_);
        out << i << "  [pos=\"" << x << "," << y << ",point\"];" << std::endl;
    }

    for (std::size_t i1 = 0; i1 < neighbor_list_.size(); i1++)
    {
        for (auto i2 : neighbor_list_[i1])
        {
            out << i1 << " -> " << i2 << std::endl;
        }
    }
    out << "}" << std::endl;
}