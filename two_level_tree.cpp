#include "solution.hpp"
#include "two_level_tree.hpp"

#include <cassert>
#include <iostream>

static constexpr std::size_t NONE = std::numeric_limits<std::size_t>::max();

template <typename T>
void print_array(const T* data, size_t size)
{
    std::cout << "[";
    for (int i = 0; i < size; i++)
    {
        std::cout << data[i] << " ";
    }
    std::cout << "]" << std::endl;
}

void TwoLevelTree::dump() const
{
    std::cout << "next:     ";
    print_array(city_next, Dimension());

    std::cout << "prev:     ";
    print_array(city_prev, Dimension());

    std::cout << "parent:   ";
    print_array(city_parent, Dimension());

    std::cout << "seg_back: ";
    print_array(segment_back, subsize + 2);

    std::cout << "seg_fr :  ";
    print_array(segment_front, subsize + 2);

    std::cout << "seg_size: ";
    print_array(segment_size, subsize + 2);

    std::cout << "seg_rev:  ";
    print_array(segment_rev, subsize + 2);

    std::cout << "seg_next: ";
    print_array(segment_next, subsize + 2);

    std::cout << "seg_prev: ";
    print_array(segment_prev, subsize + 2);
}

TwoLevelTree::TwoLevelTree(const std::vector<std::size_t>& vs)
{
    city_next = new std::size_t[vs.size()];
    city_prev = new std::size_t[vs.size()];
    city_parent = new std::size_t[vs.size()];

    const std::size_t max_subsize = ceil(sqrt(vs.size()));

    segment_back = new std::size_t[max_subsize * 2];
    segment_front = new std::size_t[max_subsize * 2];
    segment_size = new std::size_t[max_subsize * 2];
    segment_rev = new bool[max_subsize * 2];
    segment_next = new std::size_t[max_subsize * 2];
    segment_prev = new std::size_t[max_subsize * 2];

    Solution sol(vs);
    init(sol);
}

void TwoLevelTree::validate() const
{
    std::size_t initial_id = 0;
    while (city_parent[initial_id] == NONE)
    {
        initial_id++;
    }

    const std::size_t end_id = initial_id;
    int counter = 0;
    do
    {
        assert(city_parent[initial_id] != NONE);
        assert(PrevCity(NextCity(initial_id)) == initial_id);
        assert(NextCity(PrevCity(initial_id)) == initial_id);

        initial_id = NextCity(initial_id);
        counter++;
    } while (initial_id != end_id);
    assert(_size == counter);
}

TwoLevelTree::TwoLevelTree(const Solution& sol)
{
    city_next = new std::size_t[sol.Dimension()];
    city_prev = new std::size_t[sol.Dimension()];
    city_parent = new std::size_t[sol.Dimension()];

    const std::size_t max_subsize = ceil(sqrt(sol.Dimension()));

    segment_back = new std::size_t[max_subsize * 2];
    segment_front = new std::size_t[max_subsize * 2];
    segment_size = new std::size_t[max_subsize * 2];
    segment_rev = new bool[max_subsize * 2];
    segment_next = new std::size_t[max_subsize * 2];
    segment_prev = new std::size_t[max_subsize * 2];

    init(sol);
}

bool TwoLevelTree::contain(const std::size_t city_id) const
{
    return city_parent[city_id] != NONE;
}

void TwoLevelTree::init(const Solution& sol)
{
    // contain判定に使用する
    for (int i = 0; i < sol.Dimension(); i++)
    {
        city_parent[i] = NONE;
    }

    _size = sol.Dimension();
    subsize = ceil(sqrt(sol.Dimension()));

    std::vector<size_t> seg_accum_size(subsize);
    for (int i = 0; i < subsize; i++)
    {
        seg_accum_size[i] = Dimension() * (i + 1) / subsize;
    }

    segment_size[0] = seg_accum_size[0];
    for (int i = 1; i < subsize; i++)
    {
        segment_size[i] = seg_accum_size[i] - seg_accum_size[i - 1];
    }

    for (int i = 0; i < subsize - 1; i++)
    {
        segment_next[i] = i + 1;
        segment_prev[i + 1] = i;
    }
    segment_prev[0] = subsize - 1;
    segment_next[subsize - 1] = 0;

    std::size_t seg_id = 0;
    std::size_t id = 0;
    segment_front[seg_id] = id;
    for (std::size_t i = 0; i < Dimension(); i++, id = city_next[id])
    {
        if (i == seg_accum_size[seg_id])
        {
            seg_id++;
            // segment_front
            segment_front[seg_id] = id;
        }

        city_next[id] = sol.NextCity(id);
        city_prev[id] = sol.PrevCity(id);
        city_parent[id] = seg_id;

        // segment_back
        if (i + 1 == seg_accum_size[seg_id])
        {
            segment_back[seg_id] = id;
        }
    }

    for (std::size_t seg_index = 0; seg_index < subsize; seg_index++)
    {
        segment_rev[seg_index] = false;
    }
}

TwoLevelTree::~TwoLevelTree()
{
    delete[] city_prev;
    delete[] city_next;
    delete[] city_parent;

    delete[] segment_back;
    delete[] segment_front;
    delete[] segment_size;
    delete[] segment_rev;
    delete[] segment_next;
    delete[] segment_prev;
}

size_t TwoLevelTree::Dimension() const { return _size; }

std::size_t& TwoLevelTree::NextCity(const std::size_t id) const
{
    return segment_rev[city_parent[id]] ? city_prev[id] : city_next[id];
}

std::size_t& TwoLevelTree::PrevCity(const std::size_t id) const
{
    return segment_rev[city_parent[id]] ? city_next[id] : city_prev[id];
}

std::size_t& TwoLevelTree::front(const std::size_t segid)
{
    return segment_rev[segid] ? segment_back[segid] : segment_front[segid];
}

std::size_t& TwoLevelTree::back(const std::size_t segid)
{
    return segment_rev[segid] ? segment_front[segid] : segment_back[segid];
}

void TwoLevelTree::flip_insegment(const std::size_t from,
    const std::size_t to)
{
    const uint seg_from = city_parent[from];
    const uint seg_to = city_parent[to];

    if (from == front(seg_from) && to == back(seg_to))
    {
        rev_seg(seg_from, seg_to);
    }
    else if (from == front(seg_from))
    {
        const std::size_t to_end_seg = subsize;
        split(NextCity(to), to_end_seg);
        rev_seg(seg_from, seg_from);
        merge(to_end_seg);
    }
    else if (to == back(seg_from))
    {
        const std::size_t from_to_seg = subsize;
        split(from, from_to_seg);
        rev_seg(from_to_seg, from_to_seg);
        merge(from_to_seg);
    }
    else
    {
        const std::size_t from_to_seg = subsize;
        split(from, from_to_seg);
        const std::size_t to_end_seg = subsize + 1;
        split(NextCity(to), to_end_seg);

        rev_seg(from_to_seg, from_to_seg);
        merge(to_end_seg);
        merge(from_to_seg);
    }
}

void TwoLevelTree::flip_outsegment(const std::size_t from,
    const std::size_t to)
{
    const uint seg_from = city_parent[from];
    const uint seg_to = city_parent[to];

    if (from == front(seg_from) && to == back(seg_to))
    {
        rev_seg(seg_from, seg_to);
    }
    else if (from == front(seg_from))
    {
        const std::size_t to_end_seg = subsize;
        split(NextCity(to), to_end_seg);
        rev_seg(seg_from, seg_to);
        merge(to_end_seg);
    }
    else if (to == back(seg_to))
    {
        const std::size_t from_end_seg = subsize;
        split(from, from_end_seg);
        rev_seg(from_end_seg, seg_to);
        merge(from_end_seg);
    }
    else
    {
        const std::size_t from_end_seg = subsize;
        split(from, from_end_seg);

        const std::size_t to_end_seg = subsize + 1;
        split(NextCity(to), to_end_seg);

        rev_seg(from_end_seg, seg_to);

        merge(to_end_seg);
        merge(segment_prev[from_end_seg]);
    }
}

// [from, to]

void TwoLevelTree::Flip(const std::size_t from, const std::size_t to)
{
    if (segment_size[city_parent[from]] == 1 and segment_size[city_parent[to]] == 1)
    {
        rebuild();
    }

    const uint seg_from = city_parent[from];
    const uint seg_to = city_parent[to];

    if (seg_from != seg_to)
    {
        flip_outsegment(from, to);
    }
    else if (betweenInSegment(from, to, back(seg_from)))
    {
        flip_insegment(from, to);
    }
    else
    {
        flip_outsegment(from, to);
    }
}

void TwoLevelTree::rebuild()
{
    std::vector<std::size_t> vs(Dimension());
    std::size_t id = 0;
    for (int i = 0; i < Dimension(); i++)
    {
        vs[i] = id;
        id = NextCity(id);
    }
    Solution sol(vs);
    init(sol);
}

void TwoLevelTree::rev_seg(const std::size_t seg_from,
    const std::size_t seg_to)
{
    const std::size_t prev_from = segment_prev[seg_from];
    const std::size_t next_to = segment_next[seg_to];

    const std::size_t end_seg = segment_next[seg_to];
    std::size_t seg_id = seg_from;

    if (seg_id == end_seg)
    {
        // seg_id ==
        // end_segなら全部swapしきるので，余計なprev/nextの付け替えは必要なし
        do
        {
            std::swap(segment_next[seg_id], segment_prev[seg_id]);
            segment_rev[seg_id] = !segment_rev[seg_id];
            seg_id = segment_prev[seg_id];
        } while (seg_id != end_seg);
    }
    else
    {
        while (seg_id != end_seg)
        {
            std::swap(segment_next[seg_id], segment_prev[seg_id]);
            segment_rev[seg_id] = !segment_rev[seg_id];
            seg_id = segment_prev[seg_id];
        }

        // seg_from == seg_toでもOK
        segment_next[prev_from] = seg_to;
        segment_prev[next_to] = seg_from;

        segment_next[seg_from] = next_to;
        segment_prev[seg_to] = prev_from;

        NextCity(back(prev_from)) = front(seg_to);
        NextCity(back(seg_from)) = front(next_to);

        PrevCity(front(next_to)) = back(seg_from);
        PrevCity(front(seg_to)) = back(prev_from);
    }
}

// 全探索between

bool TwoLevelTree::betweenInSegment(const std::size_t a, const std::size_t b,
    const std::size_t c) const
{
    const std::size_t end_id = NextCity(c);
    std::size_t city_id = a;
    while (city_id != end_id)
    {
        if (city_id == b)
            return true;
        city_id = NextCity(city_id);
    }
    return false;
}

// 注意: flipに限定すれば，新規idはsubsize, subsize + 1で十分
//

void TwoLevelTree::split(const std::size_t city_from,
    const std::size_t new_segid)
{
    const std::size_t seg_id = city_parent[city_from];

    // segment_rev
    segment_rev[new_segid] = segment_rev[seg_id];

    const std::size_t end_id = NextCity(back(seg_id));

    // city_parent
    std::size_t city_id = city_from;
    std::size_t new_segment_size = 0;
    while (city_id != end_id)
    {
        city_parent[city_id] = new_segid;
        new_segment_size++;
        city_id = NextCity(city_id);
    }

    front(new_segid) = city_from;
    back(new_segid) = back(seg_id);

    back(seg_id) = PrevCity(city_from);

    const std::size_t next_new = segment_next[seg_id];

    // 元々seg_idの次だったやつのprevをnew_segidにする必要あり
    segment_size[new_segid] = new_segment_size;
    segment_size[seg_id] -= new_segment_size;

    segment_prev[next_new] = new_segid;
    segment_next[seg_id] = new_segid;

    segment_next[new_segid] = next_new;
    segment_prev[new_segid] = seg_id;
}

void TwoLevelTree::merge(const std::size_t seg_id1)
{
    const std::size_t seg_id2 = segment_next[seg_id1];

    assert(segment_next[seg_id1] == seg_id2);

    if (segment_rev[seg_id1] != segment_rev[seg_id2])
    {
        // if(segment_rev[seg_id1])
        if (segment_size[seg_id1] < segment_size[seg_id2])
        {
            revert_rev(seg_id1);
        }
        else
        {
            revert_rev(seg_id2);
        }
    }
    assert(segment_rev[seg_id1] == segment_rev[seg_id2]);

    if (seg_id1 > seg_id2)
    {
        const std::size_t end_id = NextCity(back(seg_id1));
        std::size_t city_id = front(seg_id1);
        while (city_id != end_id)
        {
            city_parent[city_id] = seg_id2;
            city_id = NextCity(city_id);
        }
        segment_next[segment_prev[seg_id1]] = seg_id2;
        segment_size[seg_id2] += segment_size[seg_id1];

        segment_prev[seg_id2] = segment_prev[seg_id1];
        front(seg_id2) = front(seg_id1);
    }
    else
    {
        const std::size_t end_id = NextCity(back(seg_id2));
        std::size_t city_id = front(seg_id2);
        while (city_id != end_id)
        {
            city_parent[city_id] = seg_id1;
            city_id = NextCity(city_id);
        }

        segment_prev[segment_next[seg_id2]] = seg_id1;
        segment_size[seg_id1] += segment_size[seg_id2];

        segment_next[seg_id1] = segment_next[seg_id2];
        back(seg_id1) = back(seg_id2);
    }
}

void TwoLevelTree::revert_rev(const std::size_t segid)
{
    const std::size_t end_id = NextCity(back(segid));
    std::size_t city_id = front(segid);
    while (city_id != end_id)
    {
        std::swap(NextCity(city_id), PrevCity(city_id));
        city_id = PrevCity(city_id); // swapする前のnext
    }
    segment_rev[segid] = !segment_rev[segid];
    std::swap(segment_front[segid], segment_back[segid]);
}

std::ostream& operator<<(std::ostream& out, const TwoLevelTree& tree)
{
    // 0を最初に記述する縛り
    std::size_t id = 0;
    for (int i = 0; i < tree.Dimension(); i++)
    {
        out << id << " ";
        id = tree.NextCity(id);
    }
    return out;
}
