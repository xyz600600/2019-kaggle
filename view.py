# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

xs = []
ys = []

with open('data/cities.csv') as fin:
    for line in fin.readlines()[1:]:
        tokens = line.strip().split(',')
        xs.append(float(tokens[1]))
        ys.append(float(tokens[2]))

plt.plot(xs, ys, 'b.', ms=0.5)
plt.show()
