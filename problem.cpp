#include <fstream>
#include <iostream>
#include <sstream>

#include "problem.hpp"

double EuclidDistance(const Coordinate<double>& p1, const Coordinate<double>& p2)
{
    const double y = p1.y_ - p2.y_;
    const double x = p1.x_ - p2.x_;
    return sqrt(y * y + x * x);
}

Problem::Problem(const std::string& filepath)
{
    std::fstream fin(filepath);

    std::string line;
    getline(fin, line);
    while (!fin.eof())
    {
        getline(fin, line);
        std::size_t id;
        double y, x;
        sscanf(line.c_str(), "%ld,%lf,%lf", &id, &y, &x);
        coord_list_.push_back(Coordinate(y, x));
    }
    coord_list_.pop_back();
    fin.close();
}

double Problem::Distance(const std::size_t city_i,
    const std::size_t city_j) const
{
    return EuclidDistance(coord_list_[city_i], coord_list_[city_j]);
}

double Problem::operator()(const std::size_t city_i,
    const std::size_t city_j) const
{
    return Distance(city_i, city_j);
}

std::size_t Problem::Dimension() const { return coord_list_.size(); }