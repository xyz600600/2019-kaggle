#pragma once

#include <array>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <vector>

template <typename T>
struct Coordinate
{
    T y_;
    T x_;

    Coordinate(const T y, const T x)
        : y_(y)
        , x_(x)
    {
    }

    Coordinate<T> operator+(const Coordinate& c) const;
    Coordinate<T> operator-(const Coordinate& c) const;
    Coordinate<T> operator*(T v) const;
    Coordinate<T> operator/(T v) const;
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const Coordinate<T>& v)
{
    out << "(" << v.y_ << ", " << v.x_ << ")";
    return out;
}

template <typename T>
T dot(const Coordinate<T>& c1, const Coordinate<T>& c2)
{
    return c1.y_ * c2.y_ + c1.x_ * c2.x_;
}

template <typename T>
Coordinate<T> Coordinate<T>::operator+(const Coordinate& c) const
{
    return Coordinate(y_ + c.y_, x_ + c.x_);
}

template <typename T>
Coordinate<T> Coordinate<T>::operator-(const Coordinate& c) const
{
    return Coordinate(y_ - c.y_, x_ - c.x_);
}

template <typename T>
Coordinate<T> Coordinate<T>::operator*(const T v) const
{
    return Coordinate(y_ * v, x_ * v);
}

template <typename T>
Coordinate<T> Coordinate<T>::operator/(const T v) const
{
    return Coordinate(y_ / v, x_ / v);
}

double EuclidDistance(const Coordinate<double>& p1, const Coordinate<double>& p2);

class Problem
{
public:
    explicit Problem(const std::string& filepath);

    double Distance(std::size_t city_i, std::size_t city_j) const;

    double operator()(std::size_t city_i, std::size_t city_j) const;

    std::size_t Dimension() const;

private:
    std::vector<Coordinate<double>> coord_list_;
};
