#include <cassert>
#include <iostream>
#include <random>
#include <tuple>

using namespace std;

template <typename T>
struct Node
{
    T val;
    Node* left;
    Node* right;
    double priority;
    int _size;

    Node(const T& v, const double p)
        : val(v)
        , priority(p)
        , _size(1)
    {
        left = right = nullptr;
    }

    bool is_leaf();

    Node* update();

    Node* at(const std::size_t index);

    void print(std::size_t depth);
};

template <typename T>
std::size_t size(const Node<T>* const node)
{
    return node == nullptr ? 0 : node->_size;
}

template <typename T>
bool Node<T>::is_leaf()
{
    return left == nullptr && right == nullptr;
}

template <typename T>
Node<T>* Node<T>::update()
{
    _size = 1 + size(left) + size(right);
    return this;
}

template <typename T>
Node<T>* Node<T>::at(const std::size_t index)
{
    assert(index < _size);

    if (size(left) == index)
    {
        return this;
    }
    else if (index < size(left))
    {
        return left->at(index);
    }
    else
    {
        return right->at(index - size(left) - 1);
    }
}

template <typename T>
Node<T>* Merge(Node<T>* left, Node<T>* right)
{
    if (left == nullptr)
    {
        if (right != nullptr)
        {
            return right->update();
        }
    }
    else if (right == nullptr)
    {
        return left->update();
    }

    if (left->priority > right->priority)
    {
        left->right = Merge(left->right, right);
        return left->update();
    }
    else
    {
        right->left = Merge(left, right->left);
        return right->update();
    }
}

// [0, k), [k, size) に split する
// t は破壊される
template <typename T>
std::pair<Node<T>*, Node<T>*> Split(Node<T>* t, std::size_t k)
{
    assert(k < size(t));

    if (t == nullptr)
        return make_pair(nullptr, nullptr);

    if (size(t->left) == k)
    {
        Node<T>* left = t->left;
        Node<T>* right = t->right;
        t->left = t->right = nullptr;
        return make_pair(Merge(left, t), right);
    }
    else if (k < size(t->left))
    {
        auto ts = Split(t->left, k);
        t->left = ts.second;
        return make_pair(ts.first, t->update());
    }
    else
    {
        auto ts = Split(t->right, k - size(t->left) - 1);
        t->right = ts.first;
        return make_pair(t->update(), ts.second);
    }
}

template <typename T>
void Node<T>::print(std::size_t depth)
{
    if (is_leaf())
    {
        for (int i = 0; i < depth; i++)
            cerr << "  ";
        cerr << val << " " << _size << endl;
    }
    else
    {
        if (left != nullptr)
        {
            left->print(depth + 1);
        }
        for (int i = 0; i < depth; i++)
            cerr << "  ";
        cerr << val << " " << _size << endl;

        if (right != nullptr)
        {
            right->print(depth + 1);
        }
    }
}

template <typename T>
struct Treap
{
private:
    Node<T>* root;
    mt19937 mt;
    uniform_real_distribution<double> rand;

public:
    Treap();

    void Insert(std::size_t index, const T& t);

    void Remove(std::size_t index);

    T& operator[](std::size_t index);

    const T& operator[](std::size_t index) const;

    std::size_t Size() const;

    void Print() const;
};

template <typename T>
Treap<T>::Treap()
{
    root = nullptr;
}

template <typename T>
std::size_t Treap<T>::Size() const
{
    return size(root);
}

template <typename T>
void Treap<T>::Insert(const std::size_t index, const T& t)
{
    assert(index <= Size());

    auto node = new Node<T>(t, rand(mt));
    if (index == 0)
    {
        root = Merge(node, root);
    }
    else if (index == Size())
    {
        root = Merge(root, node);
    }
    else
    {
        Node<T>*left, *right;
        std::tie(left, right) = Split(root, index - 1);
        root = Merge(Merge(left, node), right);
    }
}

template <typename T>
void Treap<T>::Remove(std::size_t index)
{
    assert(0 <= index && index < Size());

    auto p1 = Split(root, index);
    auto p2 = Split(p1.first, index - 1);
    root = Merge(p2.first, p1.second);
}

template <typename T>
const T& Treap<T>::operator[](const std::size_t index) const
{
    assert(index < Size());

    return root->at(index)->val;
}

template <typename T>
T& Treap<T>::operator[](const std::size_t index)
{
    assert(index < Size());

    return root->at(index)->val;
}

template <typename T>
void Treap<T>::Print() const
{
    if (root != nullptr)
    {
        root->print(0);
    }
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const Treap<T>& treap)
{
    out << "[";
    for (std::size_t i = 0; i < treap.Size(); i++)
    {
        if (i > 0)
        {
            out << ", ";
        }
        out << treap[i];
    }
    out << "]";
    return out;
}