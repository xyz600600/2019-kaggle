#!/bin/bash

for i in `seq 0 $1` 
do
    neato -s1 -n100 -Tpng result_${i}.dot > result_${i}.png
done
