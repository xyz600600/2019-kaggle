#include "intset.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <limits>

static constexpr std::size_t NONE = std::numeric_limits<std::size_t>::max();

IntSet::IntSet(const std::size_t dimension)
    : value2index_(dimension, NONE)
    , index2value_(dimension, NONE)
    , size_(0)
{
}

void IntSet::Push(const std::size_t value)
{
    assert(value2index_[value] == NONE);
    index2value_[size_] = value;
    value2index_[value] = size_;
    size_++;
}

void IntSet::Remove(const std::size_t value)
{
    assert(value2index_[value] != NONE);
    size_--;
    const std::size_t prev_index = value2index_[value];
    const std::size_t last_value = index2value_[size_];
    index2value_[prev_index] = last_value;
    value2index_[last_value] = prev_index;
    index2value_[size_] = NONE;
    value2index_[value] = NONE;
}

bool IntSet::Contain(const std::size_t value) const
{
    return value2index_[value] != NONE;
}

std::size_t IntSet::Size() const { return size_; }

std::size_t IntSet::RandomSelectValue(std::mt19937* mt) const
{
    assert(size_ > 0);
    const std::size_t rand = (*mt)();
    const std::size_t index = ((size_ - 1ull) * rand + (1ull << 31)) >> 32;
    return index2value_[index];
}
