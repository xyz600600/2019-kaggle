#include "../treap.hpp"

#include <gtest/gtest.h>
#include <iostream>
#include <random>
#include <stack>

TEST(test_insert_remove, treap_test)
{
    std::size_t size = 100;
    const std::size_t coef = 100;

    Treap<int> treap;
    for (int i = size; i >= 0; i--)
    {
        treap.Insert(0, i * coef);
    }
    for (int i = 0; i <= size; i++)
    {
        ASSERT_EQ(treap[i], i * coef);
    }

    std::size_t front = 30;
    std::size_t rem_size = 50;
    for (int i = 0; i < rem_size; i++)
    {
        treap.Remove(front);
        size--;
    }
    for (int i = 0; i < front; i++)
    {
        ASSERT_EQ(treap[i], i * coef);
    }
    for (int i = front; i <= size; i++)
    {
        ASSERT_EQ(treap[i], (i + rem_size) * coef);
    }
}