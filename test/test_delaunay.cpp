#include "../delaunay_graph.hpp"
#include "../graph.hpp"

#include <gtest/gtest.h>
#include <iostream>
#include <random>
#include <vector>

TEST(test_graphviz, test_delaunay)
{
    std::vector<Coordinate<double>> coords;

    std::mt19937 mt(0);
    std::uniform_real_distribution<double> rand;

    for (int i = 0; i < 10; i++)
    {
        coords.push_back(Coordinate<double>(rand(mt), rand(mt)));
        std::cout << coords.back() << std::endl;
    }

    auto graph = CalculateDelauneyGraph(coords);
    graph.to_graphviz(std::cerr, coords);
}