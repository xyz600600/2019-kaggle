#include "../solution.hpp"

#include <gtest/gtest.h>
#include <iostream>
#include <random>
#include <stack>

void ValidateSolution(const Solution& sol)
{
    for (std::size_t city = 0; city < sol.Dimension(); city++)
    {
        EXPECT_EQ(sol.NextCity(sol.PrevCity(city)), city);
        EXPECT_EQ(sol.PrevCity(sol.NextCity(city)), city);
    }
}

void Connected(const Solution& sol, const std::size_t city1, const std::size_t city2)
{
    EXPECT_EQ(sol.NextCity(city1), city2);
    EXPECT_EQ(sol.PrevCity(city2), city1);
}

TEST(test_flip, test_solution)
{
    const std::size_t dimension = 100;

    Solution sol(dimension);
    ValidateSolution(sol);

    sol.Flip(10, 20);
    ValidateSolution(sol);
    Connected(sol, 9, 20);
    for (std::size_t i = 20; i > 10; i--)
    {
        Connected(sol, i, i - 1);
    }
    Connected(sol, 10, 21);

    sol.Flip(20, 10);
    ValidateSolution(sol);
    for (std::size_t i = 0; i < dimension - 1; i++)
    {
        Connected(sol, i, i + 1);
    }
    Connected(sol, dimension - 1, 0);
}

TEST(test_random_flip, test_solution)
{
    const std::size_t dimension = 20;

    Solution sol(dimension);

    std::mt19937 mt(0);
    std::uniform_int_distribution<std::size_t> rand(0, dimension - 1);

    std::stack<std::pair<std::size_t, std::size_t>> s;
    const std::size_t max_iter = 50000;

    for (std::size_t iter = 0; iter < max_iter; iter++)
    {
        const auto from = rand(mt);
        auto to = rand(mt);
        while (from == to)
        {
            to = rand(mt);
        }
        sol.Flip(from, to);
        ValidateSolution(sol);
        s.push(std::make_pair(from, to));
    }

    while (!s.empty())
    {
        auto [from, to] = s.top();
        s.pop();
        sol.Flip(to, from);
        ValidateSolution(sol);
    }
    ValidateSolution(sol);

    for (std::size_t i = 0; i < dimension - 1; i++)
    {
        Connected(sol, i, i + 1);
    }
    Connected(sol, dimension - 1, 0);
}