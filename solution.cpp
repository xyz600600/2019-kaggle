#include <cassert>

#include "solution.hpp"

Solution::Solution(const std::vector<std::size_t>& city_list)
    : city2index_(city_list.size())
    , index2city_(city_list.size())
{
    for (std::size_t index = 0; index < city_list.size(); index++)
    {
        city2index_[city_list[index]] = index;
        index2city_[index] = city_list[index];
    }
}

Solution::Solution(const std::size_t dimension)
    : city2index_(dimension)
    , index2city_(dimension)
{
    for (std::size_t i = 0; i < dimension; i++)
    {
        city2index_[i] = index2city_[i] = i;
    }
}

std::size_t& Solution::operator[](std::size_t index)
{
    return index2city_[index];
}

const std::size_t& Solution::operator[](std::size_t index) const
{
    return index2city_[index];
}

void Solution::Flip(std::size_t from_city, std::size_t to_city)
{
    assert(from_city != to_city);
    const std::size_t iter = Distance(from_city, to_city) / 2;

    std::size_t from_index = city2index_[from_city];
    std::size_t to_index = city2index_[to_city];

    for (std::size_t i = 0; i < iter; i++)
    {
        std::swap(index2city_[from_index], index2city_[to_index]);
        std::swap(city2index_[index2city_[from_index]],
            city2index_[index2city_[to_index]]);
        from_index = NextIndex(from_index);
        to_index = PrevIndex(to_index);
    }
}

void Solution::SwapCity(const std::size_t city1, const std::size_t city2)
{
    std::swap(city2index_[city1], city2index_[city2]);
    std::swap(index2city_[city2index_[city1]], index2city_[city2index_[city2]]);
}

std::size_t Solution::NextCity(const std::size_t city) const
{
    return index2city_[NextIndex(city2index_[city])];
}

std::size_t Solution::PrevCity(const std::size_t city) const
{
    return index2city_[PrevIndex(city2index_[city])];
}

std::size_t Solution::NextIndex(const std::size_t index) const
{
    return index == city2index_.size() - 1 ? 0 : index + 1;
}

std::size_t Solution::PrevIndex(const std::size_t index) const
{
    return index == 0 ? city2index_.size() - 1 : index - 1;
}

std::size_t Solution::Distance(std::size_t city1, std::size_t city2) const
{
    const std::size_t index1 = city2index_[city1];
    const std::size_t index2 = city2index_[city2];
    return index1 < index2 ? index2 + 1 - index1
                           : index2 + Dimension() + 1 - index1;
}

std::size_t Solution::Dimension() const { return index2city_.size(); }

std::ostream& operator<<(std::ostream& out, const Solution& sol)
{
    out << "[";
    for (std::size_t i = 0; i < sol.Dimension(); i++)
    {
        if (i > 0)
        {
            out << ',';
        }
        out << sol[i];
    }
    out << "]";
    return out;
}

void Solution::Dump() const
{
    std::cout << "c2i [";
    for (auto v : city2index_)
    {
        std::cout << v << ", ";
    }
    std::cout << "]" << std::endl;

    std::cout << "i2c [";
    for (auto v : index2city_)
    {
        std::cout << v << ", ";
    }
    std::cout << "]" << std::endl;
}