#pragma once

#include "graph.hpp"
#include "problem.hpp"

#include <vector>

Graph CalculateDelauneyGraph(const std::vector<Coordinate<double>>& position_list);