#!/bin/bash

BUILD_OPTION="-Ofast -std=c++17 -Wall"

clang++ -fopenmp $BUILD_OPTION `ls | grep cpp`

clang-tidy `ls | grep hpp` `ls | grep cpp` -- $BUILD_OPTION