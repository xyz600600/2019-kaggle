#pragma once

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <vector>

static const bool debug_tree = false;

class TwoLevelTree
{
public:
    TwoLevelTree(const Solution& sol);
    TwoLevelTree(const std::vector<std::size_t>& vs);
    ~TwoLevelTree();

    std::size_t& NextCity(const std::size_t id) const;
    std::size_t& PrevCity(const std::size_t id) const;
    void Flip(const std::size_t from, const std::size_t to);
    void rebuild();
    size_t Dimension() const;
    void dump() const;

    bool contain(const std::size_t city_id) const;
    void init(const Solution& sol);

    void validate() const;

private:
    void split(const std::size_t city_from, const std::size_t new_segid);
    // 右隣りとmerge．小さいidを残す.
    void merge(const std::size_t seg_id1);

    // aとcの間にbがあるか否かを調べる
    bool betweenInSegment(const std::size_t a, const std::size_t b,
        const std::size_t c) const;
    void rev_seg(const std::size_t seg_from, const std::size_t seg_to);

    std::size_t subsize;
    std::size_t _size;

    std::size_t& front(const std::size_t segid);
    std::size_t& back(const std::size_t segid);

    void revert_rev(const std::size_t segid);

    void flip_insegment(const std::size_t from, const std::size_t to);
    void flip_outsegment(const std::size_t from, const std::size_t to);

    // city-idベースで持つ
    std::size_t* city_next;
    std::size_t* city_prev;
    std::size_t* city_parent;

    std::size_t* segment_back;
    std::size_t* segment_front;
    std::size_t* segment_size;
    bool* segment_rev;
    std::size_t* segment_next;
    std::size_t* segment_prev;
};
