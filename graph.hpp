#pragma once

#include "problem.hpp"

#include <unordered_set>
#include <vector>

class Graph
{
private:
    std::vector<std::unordered_set<std::size_t>> neighbor_list_;

public:
    Graph(std::size_t size);

    void connect(std::size_t from, std::size_t to, bool bidir = false);

    const auto& operator[](std::size_t index) const;
    auto& operator[](std::size_t index);

    void to_graphviz(std::ostream& out, const std::vector<Coordinate<double>>& position_list) const;
};