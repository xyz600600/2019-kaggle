#pragma once

#include <random>
#include <vector>

class IntSet
{
public:
    explicit IntSet(std::size_t dimension);

    void Push(std::size_t value);

    void Remove(std::size_t value);

    bool Contain(std::size_t value) const;

    std::size_t RandomSelectValue(std::mt19937* mt) const;

    std::size_t Size() const;

private:
    std::vector<std::size_t> value2index_;

    std::vector<std::size_t> index2value_;

    std::size_t size_;
};