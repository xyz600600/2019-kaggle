#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <queue>
#include <random>
#include <sstream>
#include <string>

#include "intset.hpp"
#include "problem.hpp"
#include "solution.hpp"
#include "two_level_tree.hpp"

std::size_t UniformRandom(std::size_t from, std::size_t to, std::mt19937* mt);

// [from, to)
std::size_t UniformRandom(const std::size_t from, const std::size_t to,
    std::mt19937* mt)
{
    std::size_t rand = (*mt)();
    return (((to - 1 - from) * rand + (1ull << 31)) >> 32) + from;
}

void Randomize(Solution* sol, std::mt19937* mt)
{
    for (std::size_t from = 0; from < sol->Dimension() - 1; from++)
    {
        const std::size_t to = UniformRandom(from + 1, sol->Dimension(), mt);
        sol->SwapCity((*sol)[from], (*sol)[to]);
    }
}

template <typename SolutionType>
double CalculateDistance(const SolutionType& sol, const Problem& problem)
{
    double sum = 0.0;
    std::size_t city = 0;
    for (std::size_t index = 0; index < sol.Dimension(); index++)
    {
        sum += problem.Distance(city, sol.NextCity(city));
        city = sol.NextCity(city);
    }
    return sum;
}

template <typename SolutionType>
void SaveResult(const SolutionType& sol, const std::string& filepath)
{
    std::ofstream fout(filepath);
    // print result
    fout << "Path" << std::endl;
    std::size_t city = 0;
    do
    {
        fout << city << std::endl;
        city = sol.NextCity(city);
    } while (city != 0);
    fout << city << std::endl;
    fout.close();
}

template <typename SolutionType>
void Opt2(SolutionType* sol, std::mt19937* mt, const Problem& problem,
    const std::vector<std::vector<std::size_t>>& neighbor_list)
{
    // (c1 - c2, c3 - c4) ⇒ (c1 - c3, c2 - c4) に変更
    auto calculate_gain = [&](const std::size_t c1, const std::size_t c2,
                              const std::size_t c3, const std::size_t c4) {
        return (problem.Distance(c1, c2) + problem.Distance(c3, c4)) - (problem.Distance(c1, c3) + problem.Distance(c2, c4));
    };

    double distance = CalculateDistance(*sol, problem);

    std::size_t counter = 0;
    std::uniform_real_distribution<double> rand;
    const std::size_t max_global_iter = 10000;
    double rate = 1.0;

    double min_distance = 1e100;
    double prev_distance = 1e100;

    for (std::size_t global_iter = 0; global_iter < max_global_iter;
         global_iter++)
    {
        IntSet dlb(problem.Dimension());
        for (std::size_t i = 0; i < problem.Dimension(); i++)
        {
            dlb.Push(i);
        }

        auto try_flip = [&](const std::size_t city1, const std::size_t city2,
                            const std::size_t city3, const std::size_t city4) {
            const double gain = calculate_gain(city1, city2, city3, city4);

            const double th = (max_global_iter - global_iter) * 2.5e-3 / max_global_iter * rate;
            if (gain <= 0.0 && rand(*mt) > th)
            {
                return false;
            }
            sol->Flip(city2, city3);

            distance -= gain;
            for (auto city : { city1, city2, city3, city4 })
            {
                if (!dlb.Contain(city))
                {
                    dlb.Push(city);
                }
            }
            return true;
        };

        while (dlb.Size() > 0)
        {
            const std::size_t city1 = dlb.RandomSelectValue(mt);
            assert(dlb.Contain(city1));

            bool success = false;
            for (const auto city3 : neighbor_list[city1])
            {
                {
                    const std::size_t city2 = sol->NextCity(city1);
                    const std::size_t city4 = sol->NextCity(city3);
                    if (city1 != city4 && city2 != city3)
                    {
                        success |= try_flip(city1, city2, city3, city4);
                        if (success)
                        {
                            break;
                        }
                    }
                }
                {
                    const std::size_t city2 = sol->PrevCity(city1);
                    const std::size_t city4 = sol->PrevCity(city3);
                    if (city1 != city4 && city2 != city3)
                    {
                        success |= try_flip(city2, city1, city4, city3);
                        if (success)
                        {
                            break;
                        }
                    }
                }
            }
            if (!success)
            {
                dlb.Remove(city1);
            }
            if (++counter % 50000 == 0)
            {
                if (min_distance > distance)
                {
                    min_distance = distance;
                    SaveResult(*sol, "data/submit.csv");
                }
                if (prev_distance < distance)
                {
                    // 焼き鈍しが均衡しているので、greedy 率を上げる
                    rate *= 0.9;
                }
                prev_distance = distance;

                std::cerr << "cost: " << distance << ", DLB size: " << dlb.Size()
                          << std::endl;
            }
        }
        rate /= 0.9;
        std::cerr << "result distance: " << CalculateDistance(*sol, problem)
                  << std::endl;
    }
    SaveResult(*sol, "data/submit.csv");
}

void CalculateNeighborList(const Problem& problem,
    const std::size_t no_neighbor,
    const std::string& filepath)
{
    std::vector<std::vector<std::size_t>> neighbor_list(problem.Dimension());
#pragma omp parallel for
    for (std::size_t city1 = 0; city1 < problem.Dimension(); city1++)
    {
        std::priority_queue<std::pair<double, std::size_t>> ranking;
        for (std::size_t city2 = 0; city2 < problem.Dimension(); city2++)
        {
            if (city1 != city2)
            {
                ranking.push(std::make_pair(problem.Distance(city1, city2), city2));
                if (ranking.size() > no_neighbor)
                {
                    ranking.pop();
                }
            }
        }
        while (!ranking.empty())
        {
            neighbor_list[city1].push_back(ranking.top().second);
            ranking.pop();
        }
        if (city1 % 1000 == 0)
        {
            std::cerr << city1 << " finished." << std::endl;
        }
    }
    std::ofstream fout(filepath);
    for (auto city_list : neighbor_list)
    {
        bool first = true;
        for (auto city : city_list)
        {
            if (!first)
            {
                fout << ',';
            }
            fout << city;
            first = false;
        }
        fout << std::endl;
    }
    fout.close();
}

// 距離が長い順に先頭から入っている
std::vector<std::vector<std::size_t>> LoadNeighborList(
    const std::string& filepath, const std::size_t dimension)
{
    std::vector<std::vector<std::size_t>> neighbor_list(dimension);
    std::ifstream fin(filepath);
    for (std::size_t i = 0; i < dimension; i++)
    {
        std::string line;
        getline(fin, line);
        std::stringstream ss(line);
        while (!ss.eof())
        {
            std::string city_s;
            getline(ss, city_s, ',');
            const std::size_t city = std::stoi(city_s);
            neighbor_list[i].push_back(city);
        }
    }
    return neighbor_list;
}

int main()
{
    const Problem problem("data/cities.csv");

    // CalculateNeighborList(problem, 50, "data/neighbor_list.txt");
    auto neighbor_list = LoadNeighborList("data/neighbor_list.txt", problem.Dimension());

    const std::size_t seed = 0;
    std::mt19937 mt(seed);

    Solution sol(problem.Dimension());
    TwoLevelTree tree(sol);

    Randomize(&sol, &mt);

    Opt2(&tree, &mt, problem, neighbor_list);

    return 0;
}
