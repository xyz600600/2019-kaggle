#pragma once

#include <iostream>
#include <vector>

class Solution
{
public:
    explicit Solution(std::size_t dimension);

    Solution(const std::vector<std::size_t>& city_list);

    std::size_t& operator[](std::size_t index);

    const std::size_t& operator[](std::size_t index) const;

    void Flip(std::size_t from_city, std::size_t to_city);

    std::size_t NextCity(std::size_t city) const;

    std::size_t PrevCity(std::size_t city) const;

    std::size_t Dimension() const;

    void SwapCity(std::size_t city1, std::size_t city2);

    std::size_t Distance(std::size_t city1, std::size_t city2) const;

private:
    std::vector<std::size_t> city2index_;

    std::vector<std::size_t> index2city_;

    std::size_t NextIndex(std::size_t index) const;

    std::size_t PrevIndex(std::size_t index) const;

    void Dump() const;
};

std::ostream& operator<<(std::ostream& out, const Solution& sol);
