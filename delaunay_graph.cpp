#include "delaunay_graph.hpp"
#include "treap.hpp"

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <queue>
#include <set>
#include <string>
#include <tuple>
#include <vector>

constexpr double eps = 1e-7;

enum class Event
{
    Cite,
    Circle,
};

// cite event / circle event
// cite event なら index しか使わない
struct FortuneState
{
    double y;
    Event event;
    std::size_t prev_index;
    std::size_t index;
    std::size_t next_index;

    FortuneState(const double y, std::size_t index)
        : y(y)
        , event(Event::Cite)
        , prev_index(-1)
        , index(index)
        , next_index(-1)
    {
    }

    FortuneState(const double y, std::size_t prev, std::size_t index, std::size_t next)
        : y(y)
        , event(Event::Circle)
        , prev_index(prev)
        , index(index)
        , next_index(next)
    {
    }
};

bool operator<(const FortuneState s1, const FortuneState s2)
{
    return s1.y < s2.y;
}

// 3座標の外接円の半径位置 OK?
auto GetCirclePosition(const Coordinate<double>& co1, const Coordinate<double>& co2, const Coordinate<double>& co3)
{
    const auto v = co3 - co1;
    const auto u = co2 - co1;

    const auto a = dot(v, v);
    const auto b = dot(u, v);
    const auto c = dot(u, u);

    const auto s = c * (b - a) / (2 * (b * b - a * c));
    const auto t = a * (b - c) / (2 * (b * b - a * c));

    std::cerr << "(s, t) = (" << s << ", " << t << ")" << std::endl;

    const auto relo = (v * s) + (u * t);
    const auto o = co1 + relo;
    const auto r = sqrt(dot(relo, relo));

    std::cerr << "circle: " << r << " (" << o.y_ << ", " << o.x_ << ")" << std::endl;

    return std::make_pair(o, r);
}

// 2座標の beach line の交点を返す. OK
double GetArcIntersect(const Coordinate<double>& co1, const Coordinate<double>& co2, const double y)
{
    const double x1 = co1.x_;
    const double x2 = co2.x_;
    const double y1 = co1.y_;
    const double y2 = co2.y_;

    // t = a s + b
    const double a = -(x1 - x2) / (y1 - y2);
    const double b = (y1 + y2) / 2 + (x1 * x1 - x2 * x2) / (2 * (y1 - y2));

    // s^2 + cs + d = 0
    const double c = -2 * (x2 + (y2 - y) * a);
    const double d = x2 * x2 + y2 * y2 - y * y - 2 * (y2 - y) * b;

    const double D = c * c - 4 * d;
    if (D < 0)
    {
        std::cout << "neg D = " << D << std::endl;
    }
    assert(D > 0);

    const double xmax = (-c + sqrt(D)) / 2;
    const double xmin = (-c - sqrt(D)) / 2;

    const double x = co1.y_ > co2.y_ ? xmin : xmax;

    // verification
    /**
    const Coordinate<double> lower(y, x);
    const auto [o, r] = GetCirclePosition(co1, co2, lower);

    std::cerr << "dist: " << std::endl;
    std::cerr << EuclidDistance(o, co1) << std::endl;
    std::cerr << EuclidDistance(o, co2) << std::endl;
    std::cerr << EuclidDistance(o, lower) << std::endl;
    std::cerr << r << std::endl;
    std::cerr << "--" << std::endl;
    **/

    return x;
}

Graph CalculateDelauneyGraph(const std::vector<Coordinate<double>>& position_list)
{
    Graph graph(position_list.size());

    std::priority_queue<FortuneState> event_que;
    // cite event 登録
    for (std::size_t i = 0; i < position_list.size(); i++)
    {
        event_que.push(FortuneState(position_list[i].y_, i));
    }

    // beach line の arc を左から順番に管理
    Treap<std::size_t> arc_list;

    auto condition = [&](const std::size_t i, const double sweep_height, const double x) -> bool {
        return x < GetArcIntersect(position_list[arc_list[i - 1]], position_list[arc_list[i]], sweep_height);
    };

    auto binary_search = [&](const double sweep_height, const double x) -> std::size_t {
        if (arc_list.Size() <= 1)
        {
            return 0ull;
        }
        std::size_t left = 1;
        std::size_t right = arc_list.Size() - 1;

        if (condition(left, sweep_height, x))
        {
            std::cerr << "BS 1" << std::endl;
            return left - 1;
        }
        if (!condition(right, sweep_height, x))
        {
            std::cerr << "BS 2" << std::endl;
            return right;
        }
        std::cerr << "BS 3" << std::endl;

        for (int iter = 0; iter < 64 && left + 1 < right; iter++)
        {
            std::cout << "iter = " << iter << std::endl;
            const std::size_t middle = (left + right) / 2;
            std::cout << "middle = " << middle << std::endl;
            (condition(middle, sweep_height, x) ? right : left) = middle;
        }
        return right - 1;
    };

    auto push_circle_event = [&](const std::size_t i) {
        const auto ip = arc_list[i - 1];
        const auto ic = arc_list[i];
        const auto in = arc_list[i + 1];
        const auto [c, r] = GetCirclePosition(position_list[ip], position_list[ic], position_list[in]);

        std::cerr << "(p, c, n) = (" << ip << ", " << ic << ", " << in << ")" << std::endl;
        std::cerr << "c: " << c << ", r: " << r << std::endl;
        event_que.push(FortuneState(c.y_ - r, ip, ic, in));
    };

    auto print_pos = [&](const double y) {
        std::cout << "inter: [";
        for (std::size_t i = 1; i < arc_list.Size(); i++)
        {
            std::cout << GetArcIntersect(position_list[arc_list[i - 1]], position_list[arc_list[i]], y) << " ";
        }
        std::cout << "]" << std::endl;
    };

    std::set<std::tuple<std::size_t, std::size_t, std::size_t>> deleted;

    int count = 0;

    while (!event_que.empty())
    {
        auto event = event_que.top();
        event_que.pop();

        std::cout << arc_list << std::endl;
        print_pos(event.y);

        if (event.event == Event::Cite)
        {
            std::cerr << "event cite: " << event.index << ", " << event.y << std::endl;

            // 挿入されるべき beach line を二分探索
            std::cerr << arc_list << std::endl;
            std::cerr << "target x: " << position_list[event.index].x_ << std::endl;
            const std::size_t i = binary_search(event.y, position_list[event.index].x_);
            std::cerr << "insert pos: " << i << " -> " << event.index << std::endl;

            if (1 <= i && i + 2 < arc_list.Size())
            {
                deleted.insert(std::make_tuple(arc_list[i - 1], arc_list[i], arc_list[i + 1]));
            }
            // [s_i] -> [s_i, s_i+1, s_i]
            arc_list.Insert(i, event.index);

            if (arc_list.Size() >= 2)
            {
                const std::size_t index = arc_list[i + 1];
                graph.connect(index, event.index, true);

                std::cout << arc_list << std::endl;
                std::cerr << "insert pos: " << i << " -> " << index << std::endl;

                arc_list.Insert(i, index);
                std::cout << arc_list << std::endl;

                if (1 <= i && arc_list[i - 1] != arc_list[i + 1])
                {
                    push_circle_event(i);
                }
                if (i + 4 < arc_list.Size() && arc_list[i + 1] != arc_list[i + 3])
                {
                    push_circle_event(i + 2);
                }
            }
        }
        else
        {
            std::cerr << "event circle: " << event.index << ", " << event.y << std::endl;

            auto tpl = std::make_tuple(event.prev_index, event.index, event.next_index);
            if (deleted.find(tpl) != deleted.end())
            {
                deleted.erase(tpl);
            }
            else
            {
                // 3点からなる外接円を取得
                const auto [c, r] = GetCirclePosition(position_list[event.prev_index], position_list[event.index], position_list[event.next_index]);

                // 座標位置の beach line を二分探索
                std::cerr << arc_list << std::endl;
                const auto i = binary_search(event.y + eps, c.x_ + eps);
                std::cerr << "pos = " << i << std::endl;
                std::cerr << "(p, c, n) = (" << event.prev_index << ", " << event.index << ", " << event.next_index << ")" << std::endl;

                if (1 <= i && i + 2 < arc_list.Size() && arc_list[i - 1] == event.prev_index && arc_list[i] == event.index && arc_list[i + 1] == event.next_index)
                {
                    std::cerr << "ciecle event" << std::endl;
                    graph.connect(event.prev_index, event.next_index, true);

                    std::cerr << arc_list << std::endl;

                    if (2 <= i)
                    {
                        deleted.insert(std::make_tuple(arc_list[i - 2], arc_list[i - 1], arc_list[i]));
                    }
                    deleted.insert(std::make_tuple(arc_list[i - 1], arc_list[i], arc_list[i + 1]));

                    // [s-1,s,s+1] と並んでいるなら、sを消滅
                    arc_list.Remove(i);

                    std::cerr << arc_list << std::endl;

                    // [s-2, s-1, s+1], [s-1,s+1,s+2] を監視
                    if (2 <= i && arc_list[i - 2] != arc_list[i])
                    {
                        push_circle_event(i - 1);
                    }
                    if (arc_list[i - 1] != arc_list[i + 1])
                    {
                        push_circle_event(i);
                    }
                }
            }
        }
        print_pos(event.y - 1e-9);
        std::cerr << "---" << std::endl;
        std::ofstream fout("result_" + std::to_string(count++) + ".dot");
        graph.to_graphviz(fout, position_list);
        fout.close();
    }
    return graph;
}